
import Foundation
import CoreLocation
protocol WeatherManagerDelegate{
    func didUpdateWeather (_weatherManager: WeatherManager, weather: WeatherModel)
}
struct WeatherManager{
    
    let weatherUrl = "https:api.openweathermap.org/data/2.5/weather?appid=de5f48f58c5f6e63e14fa2eb16cafcc5&units=metric"
    var delegate: WeatherManagerDelegate?
    
    func getWeather(cityName: String){ //cityName из вью контроллера
        let urlString = "\(weatherUrl)&q=\(cityName)"
        performRequest(urlString: urlString)
    }
    func getWeatherByLocation(latitude: CLLocationDegrees, longitude: CLLocationDegrees){
                                                                                          //cityName из вью контроллера
        let urlString = "\(weatherUrl)&lat=\(latitude)&lon=\(longitude)"
        performRequest(urlString: urlString)
    }
    
    func performRequest(urlString: String){
        //   1. Create a URL
        if let url = URL(string: urlString){
            //   2. Create a URL session
            let session = URLSession(configuration: .default)
            //   3. Create the session a task
            let task = session.dataTask(with: url) { (data, response, error) in
                // Получаем данные по указанному url и сессии и в data записываем
                if error != nil {
                    print(error!)
                    return
                }
                if let safeData = data{                  // парсим json
                    if let weather = self.parseJSON(weatherData: safeData){
                        self.delegate?.didUpdateWeather (_weatherManager: self, weather: weather)
                    }
                    
                }
                
            }
            //   4. Start the task
            task.resume()
            
        }
        
    }
    
    func parseJSON(weatherData: Data)->WeatherModel? {
        let decoder = JSONDecoder()
        do {
            let decodedData = try decoder.decode(WeatherData.self, from: weatherData)     // weatherData = safeData
            let temp = decodedData.main.temp
            let name = decodedData.name
            let id = decodedData.weather[0].id
            print(id)
            
            let weather = WeatherModel(temperature: temp, cityName: name, conditionID: id )
            return weather
        }
        catch{
            print(error)
            return nil
        }
        
        
        
        
    }
    
}

