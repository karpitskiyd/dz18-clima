
import UIKit
import CoreLocation

class WeatherViewController: UIViewController {
    // Важно

    @IBOutlet weak var conditionImageView: UIImageView!
    @IBOutlet weak var temperatureLabel: UILabel!
    @IBOutlet weak var cityLabel: UILabel!
    @IBOutlet weak var searchButton: UIButton!
    @IBOutlet weak var searchTextField: UITextField!
    
    var weatherManager = WeatherManager()
    let locationManager = CLLocationManager()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        locationManager.delegate = self
        locationManager.requestWhenInUseAuthorization()
        locationManager.requestLocation()
        
        
        weatherManager.delegate = self
        searchTextField.delegate = self   // связь строки с контроллером
    }
    
    
   
    
    
    }
 
extension WeatherViewController: UITextFieldDelegate {
    
    @IBAction func searchButtonPrassed(_ sender: UIButton) {
        searchTextField.endEditing(true)
        print(searchTextField.text!)
        
    }
    
    @IBAction func locationButtonPrassed(_ sender: UIButton) {
        locationManager.requestLocation()
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        searchTextField.endEditing(true)
        print(searchTextField.text!)
        return true
    }
                        
    func textFieldShouldEndEditing(_ textField: UITextField) -> Bool {
        if searchTextField.text != "" {
                return true
            }
        else {
            searchTextField.placeholder = "Type something ..." // серая надпись на заднем плане строки
            return false
        }
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        if let city = searchTextField.text{
            weatherManager.getWeather(cityName: city)

        }
        
        searchTextField.text = ""
    }
    
    
    
    
    
}
    
extension WeatherViewController: WeatherManagerDelegate{
    func didUpdateWeather (_weatherManager: WeatherManager, weather: WeatherModel){
        DispatchQueue.main.async {
            self.conditionImageView.image = UIImage(systemName: weather.conditionName)
            self.temperatureLabel.text = weather.temeratureString
            self.cityLabel.text = weather.cityName
            
        }
    }
}
extension WeatherViewController: CLLocationManagerDelegate{
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        if let location = locations.last{
            locationManager.stopUpdatingLocation()
            let lat = location.coordinate.latitude
            let lon = location.coordinate.longitude
            print("location(lat:\(lat) lon:\(lon))")
            weatherManager.getWeatherByLocation(latitude: lat, longitude: lon)
        }
      
    }
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        print("Error")
    }
}
         
    


